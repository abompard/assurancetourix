Assurancetourix
===============

Assurancetourix is a web application that can manage your Karaoke party.

- Download it from PyPI: https://pypi.python.org/pypi/assurancetourix
- Get the source and report bugs on GitLab: https://gitlab.com/abompard/assurancetourix

Assurancetourix is licensed under the `Affero GPL v3`_ or any later version.

.. _`Affero GPL v3`: http://www.gnu.org/licenses/agpl-3.0.html


Installation
------------

If you are unfamiliar with the way Flask applications are usually deployed,
check out `the official documentation`_ on Flask's website.

.. _`the official documentation`: http://flask.pocoo.org/docs/dev/deploying/


Credits
-------

This project is based on `Flask`_ and `jQuery Mobile`_.
Microphone icon by `Alex Auda Samora`_ from the Noun Project.
Icons from the `Font Awesome`_ project.


.. _Flask: http://flask.pocoo.org/
.. _jQuery Mobile: http://jquerymobile.com/
.. _Alex Auda Samora: https://thenounproject.com/razerk/
.. _Font Awesome: http://fortawesome.github.io/Font-Awesome/


TODO
----

- A "Popular" tab where the most played songs would be displayed. API endpoint
  to upload Performous' ``database.xml`` file. Store each karaoke session in
  the DB with the tracks that were sung, and build the hall of fame from there.
- Add a link to ultrastar-es.org
