from flask import g, request
from flask_babel import Babel, get_locale

LANGUAGES = []


def _get_accepted_languages():
    global LANGUAGES
    if not LANGUAGES:
        LANGUAGES = [locale.language for locale in babel.list_translations()]
        LANGUAGES.sort()
    return LANGUAGES


def pick_locale():
    return request.accept_languages.best_match(_get_accepted_languages())


def get_timezone():
    user = getattr(g, "user", None)
    if user is not None:
        return user.timezone


def store_locale():
    # Store the current locale in g for access in the templates.
    g.locale = get_locale()


babel = Babel(locale_selector=pick_locale, timezone_selector=get_timezone)
