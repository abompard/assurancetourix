"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import os

import jinja2
from flask import Flask

from assurancetourix import l10n
from assurancetourix.api import api_bp
from assurancetourix.database import db
from assurancetourix.importer import cli
from assurancetourix.views import main_bp


def create_app(config=None):
    config = config or {}

    app = Flask(__name__)

    # Load default configuration
    app.config.from_object("assurancetourix.defaults")
    # Load the optional configuration file
    if "FLASK_SETTINGS" in os.environ:
        app.config.from_envvar("FLASK_SETTINGS")
    # Load the config passed as argument
    app.config.update(config or {})

    # Logging
    if not app.debug:
        from logging import StreamHandler

        handler = StreamHandler()
        handler.setLevel(logging.WARNING)
        app.logger.addHandler(handler)

    # Multiple template folders
    if app.config["TEMPLATE_DIRS"]:
        app.jinja_loader = jinja2.ChoiceLoader(
            [jinja2.FileSystemLoader(app.config["TEMPLATE_DIRS"]), app.jinja_loader]
        )

    # Extensions
    l10n.babel.init_app(
        app, locale_selector=l10n.pick_locale, timezone_selector=l10n.get_timezone
    )
    app.before_request(l10n.store_locale)
    app.jinja_env.add_extension("jinja2.ext.i18n")
    db.init_app(app)

    @app.context_processor
    def inject_covers_dir():
        return dict(
            covers_dir=app.config["COVERS_DIR"], covers_size=app.config["COVERS_SIZE"]
        )

    # Views
    app.register_blueprint(api_bp, url_prefix="/api")
    app.register_blueprint(main_bp)

    # CLI
    app.cli.add_command(cli)

    return app
