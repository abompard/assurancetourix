import os
import re

from flask import current_app, request

SHORTTITLE_RE = re.compile(r"^(.*)\s+[\(\[].*[\)\]]$")


def get_cover_folder():
    return os.path.join(current_app.static_folder, current_app.config["COVERS_DIR"])


def limit_results(results):
    total = results.count()
    try:
        start = int(request.args.get("start", "0"))
    except (TypeError, ValueError):
        start = 0
    end = start + current_app.config["DISPLAY_BATCH_SIZE"]
    if end >= total:
        next_start = None
    else:
        next_start = end
    return results[start:end], next_start


def get_comparable_title(title):
    mo = SHORTTITLE_RE.match(title)
    if mo is not None:
        title = mo.group(1)
    return title.lower()
