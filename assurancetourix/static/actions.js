/* Progressive loading */
function progressive_load(obj, url) {
    var full_url,
        spinner = obj.find(".spinner"),
        sep = url.indexOf("?") !== -1 ? "&" : "?",
        deferred = $.Deferred();
    function load_next(start) {
        full_url = url + sep + "start=" + start;
        spinner.show();
        $.ajax({
            dataType: "json",
            url: full_url,
            success: function(data) {
                var html = $(data.html);
                spinner.before(html);
                html.find("img.lazy").lazyload({
                    threshold : 200,
                    //effect : "fadeIn",
                    skip_invisible : true
                });
                if (data.next) {
                    window.setTimeout(function() {
                        load_next(data.next);
                    }, 100);
                } else {
                    deferred.resolve();
                }
            },
            complete: function(data) {
                spinner.hide();
            }
        });
    }
    load_next(0);
    return deferred.promise();
}


/* Artist cover switching */
function switch_artist_cover(list_obj) {
    var artist_index = 0,
        cover_index = 1,
        artist_batch = 10,
        interval = 1000;
    function switch_covers() {
        var artists = list_obj.find(".artist-info .song-cover")
                              .filter(function() {
                                  return $(this).find("img").length > 1;
                              }),
            current_artists = artists.slice(artist_index, artist_index + artist_batch);
        // Switch the covers with a fadeout
        current_artists.each(function() {
            var covers = $(this).find("img"),
                next_index = cover_index % covers.length;
            covers.filter("img:visible").fadeOut("slow", function() {
                // Show the cover (must be done first or the lazy loader can't know if it's in the viewport)
                covers.eq(next_index).show();
                // Play nice with lazy loading
                $(window).trigger("scroll");
            });
        });
        // When all covers are switched, jump to the next batch.
        current_artists.find("img").promise().done(function() {
            artist_index += artist_batch;
            if (artist_index >= artists.length) {
                // We've run through all artists, do it again with the next
                // cover.
                artist_index = 0;
                cover_index += 1;
                window.setTimeout(switch_covers, interval);
            } else {
                window.setTimeout(switch_covers, 100);
            }
        });
    }
    switch_covers();
}


/* Main */
$(function() {

    /* Back-to-top button */
    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });

    // Progressive loading of the song list.
    $("#songlist").each(function() {
        var songlist = $(this);
        progressive_load(
                songlist, songlist.attr("data-load-from")
            ).done(function() {
                // Start switching covers when all songs are loaded for
                // performance reasons.
                switch_artist_cover(songlist);
            });
    });

});
