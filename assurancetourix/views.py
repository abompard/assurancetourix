"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from urllib.parse import urlencode

from flask import Blueprint, json, redirect, render_template, request, url_for
from sqlalchemy import desc, func

from .database import db
from .models import Artist, Genre, Language, PlaylistEntry, SearchTerm, Song
from .utils import limit_results

main_bp = Blueprint("main", __name__)


@main_bp.route("/", methods=["GET"])
def index():
    return redirect(url_for(".by_genre"))


@main_bp.route("/by-artist", methods=["GET"])
def by_artist():
    return render_template(
        "index.html", page_id="by-artist", load_url=url_for(".data_by_artist")
    )


@main_bp.route("/latest", methods=["GET"])
def latest():
    return render_template(
        "index.html", page_id="latest", load_url=url_for(".data_songs", order="latest")
    )


@main_bp.route("/by-genre", methods=["GET"])
def by_genre():
    genre = request.args.get("genre", "").strip()
    genres = (
        db.session.query(Genre.name, func.count(Song.song_id))
        .join(Song)
        .group_by(Genre.name)
        .order_by(Genre.name)
        .all()
    )
    return render_template(
        "by-genre.html",
        page_id="by-genre",
        genres=genres,
        genre=genre,
        load_url=url_for(".data_songs", genre=genre),
    )


@main_bp.route("/by-language", methods=["GET"])
def by_language():
    language = request.args.get("language", "").strip()
    languages = (
        db.session.query(Language.name, func.count(Song.song_id))
        .join(Song)
        .group_by(Language.name)
        .order_by(Language.name)
        .all()
    )
    return render_template(
        "by-language.html",
        page_id="by-language",
        languages=languages,
        language=language,
        load_url=url_for(".data_songs", language=language),
    )


@main_bp.route("/search", methods=["GET"])
def search():
    q = request.args.get("q", "")
    duo = request.args.get("duo", type=bool)
    karaoke = request.args.get("karaoke", type=bool)
    qs = {}
    if q:
        qs["q"] = q
    if duo:
        qs["duo"] = "on"
    if karaoke:
        qs["karaoke"] = "on"
    load_url = "{}?{}".format(url_for(".data_search"), urlencode(qs))
    return render_template(
        "index.html",
        page_id="search",
        search_query=q,
        duo=duo,
        karaoke=karaoke,
        load_url=load_url,
    )


@main_bp.route("/popular", methods=["GET"])
def popular():
    return render_template(
        "index.html", page_id="popular", load_url=url_for(".data_popular")
    )


@main_bp.route("/data/by-artist", methods=["GET"])
def data_by_artist():
    genre = request.args.get("genre", "").strip()
    language = request.args.get("language", "").strip()

    artists = db.session.query(Artist).order_by(Artist.sort_as)
    if genre or language:
        artists = artists.join(Song)
        if genre:
            artists = artists.join(Genre).filter(Genre.name == genre)
        if language:
            artists = artists.join(Language).filter(Language.name == language)
    artists, next_start = limit_results(artists)
    return json.jsonify(
        next=next_start, html=render_template("_artist_list.html", artists=artists)
    )


@main_bp.route("/data/latest", methods=["GET"])
def data_songs():
    genre = request.args.get("genre", "").strip()
    language = request.args.get("language", "").strip()
    order = request.args.get("order", "").strip()

    songs = db.session.query(Song)

    if order == "latest":
        songs = songs.order_by(desc(Song.date_added))
    if genre or language:
        songs = songs.join(Artist).order_by(Artist.name, Song.title)
        if genre:
            songs = songs.join(Genre).filter(Genre.name == genre)
        if language:
            songs = songs.join(Language).filter(Language.name == language)
    songs, next_start = limit_results(songs)
    return json.jsonify(
        next=next_start, html=render_template("_song_list.html", songs=songs)
    )


@main_bp.route("/data/popular", methods=["GET"])
def data_popular():
    popularity = (
        db.session.query(
            Song.song_id, func.count(PlaylistEntry.song_id).label("popularity")
        )
        .join(PlaylistEntry)
        .group_by(Song.song_id)
        .subquery()
    )
    entries = (
        db.session.query(Song, popularity.c.popularity)
        .join(popularity, Song.song_id == popularity.c.song_id)
        .order_by(desc(popularity.c.popularity), Song.song_id)
    )
    entries, next_start = limit_results(entries)
    return json.jsonify(
        next=next_start,
        html=render_template("_song_list_popularity.html", entries=entries),
    )


@main_bp.route("/data/search", methods=["GET"])
def data_search():
    q = request.args.get("q", "").strip()
    duo = request.args.get("duo", type=bool)
    karaoke = request.args.get("karaoke", type=bool)
    songs = db.session.query(Song).join(Artist)
    if not q and not duo and not karaoke:
        return json.jsonify(html="")
    if q:
        songs = (
            songs.join(SearchTerm)
            .filter(SearchTerm.terms.like(f"%{q.lower()}%"))
            .distinct()
        )
    if duo:
        songs = songs.filter(Song.duo.is_(True))
    if karaoke:
        songs = songs.filter(Song.has_karaoke.is_(True))
    songs = songs.order_by(Artist.sort_as, Song.title)
    songs, next_start = limit_results(songs)
    return json.jsonify(
        next=next_start, html=render_template("_song_list.html", songs=songs)
    )
