"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import datetime
import os
import re
import unicodedata

import flask
import PIL
import PIL.Image
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Unicode,
    UniqueConstraint,
    event,
)
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import false, true

from assurancetourix.database import Base
from assurancetourix.utils import get_cover_folder

SORT_ALLOWED = re.compile(r"[^\w ]")


def get_sort_string(value):
    value = SORT_ALLOWED.sub("", value.strip().lower())
    for ignore in ["the ", "les "]:
        if value.startswith(ignore):
            value = value[len(ignore) :]
    return value


def strip_accents(s):
    # http://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-in-a-python-unicode-string
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


class Artist(Base):

    __tablename__ = "artists"

    artist_id = Column("id", Integer, primary_key=True)
    name = Column(Unicode(254), index=True, unique=True, nullable=False)
    sort_as = Column(
        Unicode(254),
        index=True,
        nullable=False,
        default=lambda c: get_sort_string(c.current_parameters["name"]),
    )


class Song(Base):

    __tablename__ = "songs"
    __table_args__ = (UniqueConstraint("artist_id", "title"),)

    song_id = Column("id", Integer, primary_key=True)
    artist_id = Column(
        Integer,
        ForeignKey("artists.id", ondelete="cascade", onupdate="cascade"),
        nullable=False,
    )
    artist = relationship("Artist", backref="songs", lazy="joined")
    title = Column(Unicode(254), index=True, nullable=False)
    genre_id = Column(
        Integer,
        ForeignKey("genres.id", ondelete="set null", onupdate="cascade"),
        nullable=True,
    )
    genre = relationship("Genre", backref="songs", lazy="joined")
    language_id = Column(
        Integer,
        ForeignKey("languages.id", ondelete="set null", onupdate="cascade"),
        nullable=True,
    )
    language = relationship("Language", backref="songs", lazy="joined")
    image = Column(Unicode(254), index=True, nullable=True)
    active = Column(Boolean, server_default=true(), nullable=False)
    date_added = Column(
        DateTime, default=datetime.datetime.utcnow, index=True, nullable=False
    )
    date_updated = Column(
        DateTime, default=datetime.datetime.utcnow, index=True, nullable=False
    )
    duo = Column(Boolean, server_default=false(), nullable=False)
    has_karaoke = Column(Boolean, server_default=false(), nullable=False)
    search_terms = relationship(
        "SearchTerm", backref="song", cascade="all, delete-orphan"
    )
    playlist_entries = relationship(
        "PlaylistEntry",
        back_populates="song",
        lazy="joined",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    # parties = association_proxy("playlist_entries", "party")

    @property
    def cover(self):
        cover_folder = get_cover_folder()
        if not self.image or not os.path.exists(os.path.join(cover_folder, self.image)):
            return None
        return flask.url_for(
            "static",
            filename="/".join([flask.current_app.config["COVERS_DIR"], self.image]),
        )

    @cover.setter
    def cover(self, fileobj):
        cover_folder = get_cover_folder()
        image_filename = f"{self.song_id}.jpg"
        id_as_str = str(self.song_id)
        subdirs = []
        subdirs_count = max(0, len(str(self.song_id)) - 2)
        for i in range(subdirs_count):
            if len(id_as_str) > i:
                subdirs.append(id_as_str[i])
        image_dir = os.path.join(cover_folder, *subdirs)
        image_path = os.path.join(image_dir, image_filename)
        if fileobj is None:
            if os.path.exists(image_path):
                os.remove(image_path)
            self.image = None
            return
        # Resize the image
        if not os.path.isdir(image_dir):
            os.makedirs(image_dir)
        img = PIL.Image.open(fileobj.stream)
        img_width, img_height = img.size
        if (
            img_width > flask.current_app.config["COVERS_SIZE"]
            or img_height > flask.current_app.config["COVERS_SIZE"]
        ):
            img.thumbnail(
                (
                    flask.current_app.config["COVERS_SIZE"],
                    flask.current_app.config["COVERS_SIZE"],
                ),
                PIL.Image.ANTIALIAS,
            )
        with open(image_path, "w") as fh:
            img.save(fh, "JPEG")
        # Don't use os.path.join below, it's an URL
        self.image = "/".join(subdirs + [image_filename])

    def _delete_cover(self):
        if not self.image:
            return
        cover_folder = get_cover_folder()
        image_path = os.path.join(cover_folder, self.image.replace("/", os.sep))
        try:
            os.remove(image_path)
        except OSError:
            pass  # already removed?
        subdirs = self.image.split("/")
        while subdirs:
            subdirs.pop(-1)
            subdir_path = os.path.join(cover_folder, *subdirs)
            if not os.path.exists(subdir_path):
                continue
            if not os.listdir(subdir_path):
                os.rmdir(subdir_path)

    def update_search_terms(self):
        self.search_terms = []
        terms = []
        for text in (self.artist.name, self.title):
            terms.append(text.lower())
            terms.append(strip_accents(text.lower()))
        if self.genre:
            terms.append(self.genre.name.lower())
            terms.append(strip_accents(self.genre.name.lower()))
        if self.language:
            terms.append(self.language.name.lower())
            terms.append(strip_accents(self.language.name.lower()))
        self.search_terms = [
            SearchTerm(song_id=self.song_id, terms=term) for term in terms
        ]


@event.listens_for(Song, "after_delete")
def receive_after_delete(mapper, connection, target):
    target._delete_cover()


class SearchTerm(Base):

    __tablename__ = "search_terms"

    id = Column(Integer, primary_key=True)
    song_id = Column(
        Integer,
        ForeignKey("songs.id", ondelete="cascade", onupdate="cascade"),
        nullable=False,
    )
    terms = Column(Unicode(254), index=True, nullable=False)


class Genre(Base):

    __tablename__ = "genres"

    genre_id = Column("id", Integer, primary_key=True)
    name = Column(Unicode(254), index=True, unique=True, nullable=False)


class Language(Base):

    __tablename__ = "languages"

    id = Column("id", Integer, primary_key=True)
    name = Column(Unicode(254), index=True, unique=True, nullable=False)


class PlaylistEntry(Base):

    __tablename__ = "playlist_entries"

    song_id = Column(
        Integer,
        ForeignKey("songs.id", ondelete="cascade", onupdate="cascade"),
        primary_key=True,
        nullable=False,
        index=True,
    )
    party_id = Column(
        Integer,
        ForeignKey("parties.id", ondelete="cascade", onupdate="cascade"),
        primary_key=True,
        nullable=False,
        index=True,
    )
    order = Column("order", Integer, primary_key=True, index=True, nullable=False)
    song = relationship("Song", back_populates="playlist_entries", lazy="joined")
    party = relationship("Party", back_populates="playlist", lazy="joined")


class Party(Base):

    __tablename__ = "parties"

    id = Column("id", Integer, primary_key=True)
    date = Column(Date, default=datetime.datetime.today, index=True, nullable=False)
    playlist = relationship(
        "PlaylistEntry",
        back_populates="party",
        lazy="joined",
        order_by="PlaylistEntry.order",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    # songs = association_proxy("playlist", "song")

    def __marshallable__(self):
        retval = self.__dict__.copy()
        retval["party_id"] = self.id
        return retval

    @property
    def party_id(self):
        # Help Flask-Restful generate URLs which use party_id in their placeholders
        return self.id
