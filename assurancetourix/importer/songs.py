#!/usr/bin/env python3

"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
from codecs import BOM_UTF8
from datetime import datetime, timezone
from io import BytesIO

# import dateutil.parser
import PIL
import PIL.Image

from assurancetourix.utils import get_comparable_title

from .display import ProgressReporter, StatsManager

COVERS_SIZE = 70


class SongsImporter:
    def __init__(
        self,
        client,
        directories=None,
        add_only=False,
        dry_run=False,
        verbose=False,
    ):
        self.client = client
        self.directories = directories or []
        self.add_only = add_only
        self.dry_run = dry_run
        self.stats = StatsManager()
        self.reporter = ProgressReporter(verbose)
        self._all_songs_cache = None

    def run(self):
        seen_ids = []
        for directory in self.directories:
            seen_ids.extend(self.import_dir(directory))
        if not self.add_only:
            all_song_ids = [s["id"] for s in self.all_songs]
            deleted_songs_ids = set(all_song_ids) - set(seen_ids)
            deleted_songs = [s for s in self.all_songs if s["id"] in deleted_songs_ids]
            for song in deleted_songs:
                self.reporter.song_deleted(song)
                if self.dry_run:
                    continue
                self.client.call_api("songs/{}".format(song["id"]), method="DELETE")
            self.reporter.songs_cleaned_up()
            self.stats.add("deleted", len(deleted_songs))
        self.stats.print_stats()

    @property
    def all_songs(self):
        if self._all_songs_cache is None:
            self._all_songs_cache = self.client.call_api("songs/", params={"max": 200})
            for song in self._all_songs_cache:
                song["comparable_title"] = get_comparable_title(song["title"])
        return self._all_songs_cache

    def import_dir(self, directory):
        seen_ids = []
        self.reporter.directory_import_started(directory)
        for root, dirs, files in os.walk(directory):
            dirs = [d for d in dirs if not d.startswith(".")]
            for f in files:
                if os.path.splitext(f)[1] != ".txt":
                    continue
                song = self.process_file(os.path.join(root, f))
                if song:
                    seen_ids.append(song["id"])
        self.reporter.directory_imported()
        return seen_ids

    def process_file(self, path):
        song_data = self._get_song_data(path)
        if song_data is None:
            self.reporter.song_skipped()
            return
        song = self._upload_song_to_server(song_data)
        self._process_song_image(song, song_data)
        return song

    def _get_song_data(self, path):
        data = {"path": path}
        with open(data["path"], "rb") as fh:
            for line in fh:
                if line.startswith(BOM_UTF8):
                    # print("BOM detected in", data["path"])
                    line = line[len(BOM_UTF8) :]
                    line = line.decode("utf-8")
                else:
                    try:
                        line = line.decode("utf-8")
                    except UnicodeDecodeError:
                        line = line.decode("iso-8859-1")
                tags = (
                    "artist",
                    "title",
                    "language",
                    "genre",
                    "cover",
                    "background",
                    "video",
                    "duetsingerp1",
                    "duetsingerp2",
                )
                for tag in tags:
                    if line.startswith("#%s:" % tag.upper()):
                        data[tag] = ":".join(line.split(":")[1:]).strip()
        if "artist" not in data:
            print(data["path"], "has no artist")
            return
        if "title" not in data:
            print(data["path"], "has no title")
            return
        if data["title"].endswith(" (*)"):
            data["title"] = data["title"][:-4]
            data["flawed"] = True
        # Filter songs
        title_lower = data["title"].lower()
        if title_lower.endswith(" (karaoke)") or title_lower.endswith(" (karaoké)"):
            data["title"] = data["title"][:-10]
            data["karaoke"] = True
        return data

    def _upload_song_to_server(self, data):
        mtime = int(os.path.getmtime(data["path"]))
        # Song
        params = {
            "artist": data["artist"],
            "title": data["title"],
            "comparable_title": get_comparable_title(data["title"]),
        }
        # songs = self.client.call_api("songs/search", params=params)
        songs = [
            s
            for s in self.all_songs
            if s["artist"]["name"] == data["artist"]
            and s["comparable_title"] == params["comparable_title"]
        ]
        if songs:
            current = songs[0]
            if (
                current["title"] != data["title"]
                and current["comparable_title"] == params["comparable_title"]
            ):
                # Found a variant, shortening title to comparable_title
                params["title"] = params["comparable_title"]
        else:
            current = {}
        params["date_updated"] = mtime
        # Genre
        params["genre"] = data.get("genre")
        params["language"] = data.get("language")
        params["duo"] = "duetsingerp1" in data and "duetsingerp2" in data
        params["has_karaoke"] = "karaoke" in data
        new = current.copy()
        new.update(params)
        new["artist"] = {"name": params["artist"]}
        if not current.get("id"):
            params["date_added"] = mtime
            self.reporter.song_added(new)
            current = self._server_add(params)
        elif self._compare_songs(current, params):
            self.stats.add("unchanged")
            self.reporter.song_unchanged(new)
        else:
            self.reporter.song_updated(new)
            current = self._server_update(current, params)
        return current

    def _compare_songs(self, song1, song2):
        params = (
            "artist",
            "title",
            "date_updated",
            "genre",
            "language",
            "duo",
            "has_karaoke",
        )

        def cmpcopy(song):
            result = {}
            for param in params:
                value = song[param]
                if param == "date_updated":
                    if isinstance(song[param], int):
                        value = datetime.fromtimestamp(song[param], tz=timezone.utc)
                    if isinstance(song[param], str):
                        value = datetime.fromisoformat(song[param]).replace(
                            tzinfo=timezone.utc
                        )
                if param == "artist" and isinstance(song[param], dict):
                    value = song[param]["name"]
                if param == "genre" and song[param] == "":
                    value = None
                result[param] = value
            return result

        # if (
        #    cmpcopy(song1) != cmpcopy(song2)
        #    and song1["has_karaoke"] == song2["has_karaoke"]
        #    and song1["duo"] == song2["duo"]
        # ):
        #    print(cmpcopy(song1), cmpcopy(song2))
        # if (
        #    cmpcopy(song1) == cmpcopy(song2)
        #    and song1["has_karaoke"] == song2["has_karaoke"]
        #    and song1["duo"] == song2["duo"]
        # ):
        #     print("Equal:", cmpcopy(song1), cmpcopy(song2))
        return cmpcopy(song1) == cmpcopy(song2)

    def _prepare_song_categories(self, params):
        if self.dry_run:
            return
        self.client.get_or_create("artists", {"name": params["artist"]})
        if params["genre"]:
            self.client.get_or_create("genres", {"name": params["genre"]})
        if params["language"]:
            self.client.get_or_create("languages", {"name": params["language"]})

    def _server_add(self, params):
        if self.dry_run:
            current = params.copy()
            current["id"] = 0
            current["artist"] = {"name": current.pop("artist")}
        else:
            self._prepare_song_categories(params)
            current = self.client.call_api("songs/", data=params, method="POST")
            current["comparable_title"] = get_comparable_title(current["title"])
        if self._all_songs_cache is not None:
            self._all_songs_cache.append(current)
        self.stats.add("added")
        return current

    def _server_update(self, current, params):
        for param in ("has_karaoke", "duo"):
            # Don't overwrite if we also have the non-karaoke or non-duo
            # version.
            if current.get(param, False) and not params.get(param, False):
                return current
        # Possible optimization, but be careful with karaoke and duet songs
        # that have an mtime different from the main song file.
        # date_updated = dateutil.parser.parse(current["date_updated"])
        # date_file = datetime.fromtimestamp(
        #     params["date_updated"], tz=timezone.utc).replace(microsecond=0)
        # if date_updated < date_file:
        if not self.dry_run:
            self._prepare_song_categories(params)
            current = self.client.call_api(
                "songs/{}".format(current["id"]), data=params, method="PATCH"
            )
            current["comparable_title"] = get_comparable_title(current["title"])
        self.stats.add("updated")
        return current

    def _process_song_image(self, song, data):
        if self.dry_run:
            return
        # Process the image
        if song["cover"]:
            return  # Don't overwrite yet
        for tag in ["cover", "background"]:
            if tag not in data:
                continue
            image_path = os.path.realpath(
                os.path.join(os.path.dirname(data["path"]), data[tag])
            )
            if not os.path.exists(image_path):
                continue
            # Resize the image
            img = PIL.Image.open(image_path)
            img.thumbnail((COVERS_SIZE, COVERS_SIZE), PIL.Image.LANCZOS)
            image = BytesIO()
            if img.mode in ("RGBA", "P"):
                img = img.convert("RGB")
            img.save(image, "JPEG")
            image.seek(0)
            self.client.call_api(
                "songs/{}".format(song["id"]), files={"cover": image}, method="PATCH"
            )
            break
