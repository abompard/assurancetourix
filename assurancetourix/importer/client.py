import time
from urllib.parse import urljoin

import requests
from requests.packages import urllib3


class ConnectionError(Exception):
    def __str__(self):
        return self.args[0]


class APIError(Exception):
    def __init__(self, response):
        self.response = response
        if response.headers["Content-Type"] == "application/json":
            self.msg = response.json()["message"]
        else:
            self.msg = response.text

    def __str__(self):
        return str(self.msg)


class APIClient:
    def __init__(
        self,
        url,
        token=None,
        ignore_ssl_errors=False,
        verbose=False,
    ):
        self.url = url
        self.token = token
        self.verbose = verbose
        self.ignore_ssl_errors = ignore_ssl_errors
        if self.ignore_ssl_errors:
            urllib3.disable_warnings()

    def call_api(self, endpoint, **kwargs):
        def _is_paginated(result):
            return isinstance(result.get("results"), list) and "total" in result

        api_url = urljoin(self.url, "api/v1/")
        url = urljoin(api_url, endpoint)
        common_kwargs = {"timeout": 60}
        if self.token:
            common_kwargs["headers"] = {"Authorization": f"Bearer {self.token}"}
        if self.ignore_ssl_errors:
            common_kwargs["verify"] = False
        kwargs.update(common_kwargs)
        method = kwargs.pop("method", "GET")
        try:
            response = requests.request(method, url, **kwargs)
        except requests.exceptions.ConnectionError:
            raise ConnectionError("Could not connect to server")
        if response.status_code == 404 and method == "GET":
            return None
        if response.status_code == 204:
            return None  # 204 NO CONTENT
        if response.status_code == 502:
            # Bad Gateway, may be temporary
            retries = 5
            while retries > 0:
                time.sleep(1)
                print("Server failed, retrying...")
                response = requests.request(method, url, **kwargs)
                if response.ok:
                    break
                retries -= 1
        if not response.ok:
            raise APIError(response)
        result = response.json()
        if _is_paginated(result):
            results = result["results"]
            while result.get("next_url"):
                response = requests.get(  # noqa: S113
                    result["next_url"], **common_kwargs
                )
                if not response.ok:
                    raise APIError(response)
                result = response.json()
                results.extend(result["results"])
            return results
        else:
            return result

    def get_or_create(self, endpoint, params):
        obj = self.call_api(endpoint + "/search", params=params)
        if not obj:
            obj = self.call_api(endpoint + "/", data=params, method="POST")
            return obj
        else:
            return obj[0]
