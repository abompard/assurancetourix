import os


class ProgressReporter:
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.seen = 0
        self.total = 0

    def song_added(self, song):
        self.seen += 1
        if self.verbose:
            print("\rAdding {} - {}".format(song["artist"]["name"], song["title"]))
            self.print_progress()
        else:
            print("a", end="", flush=True)

    def song_updated(self, song):
        self.seen += 1
        if self.verbose:
            print("\rUpdating {} - {}".format(song["artist"]["name"], song["title"]))
            self.print_progress()
        else:
            print("u", end="", flush=True)

    def song_unchanged(self, song):
        self.seen += 1
        if self.verbose:
            print("\rUnchanged: {} - {}".format(song["artist"]["name"], song["title"]))
            self.print_progress()
        else:
            print("-", end="", flush=True)

    def song_deleted(self, song):
        if self.verbose:
            print("\rDeleting {} - {}".format(song["artist"]["name"], song["title"]))
        else:
            print("d", end="", flush=True)

    def song_skipped(self):
        self.seen += 1
        if self.verbose:
            print("\r", end="", flush=True)
            self.print_progress()

    def directory_imported(self):
        print()

    def songs_cleaned_up(self):
        print()

    def directory_import_started(self, directory):
        for root, dirs, files in os.walk(directory):
            dirs = [d for d in dirs if not d.startswith(".")]
            self.total += len([f for f in files if os.path.splitext(f)[1] == ".txt"])
        if self.verbose:
            self.print_progress()

    def print_progress(self):
        print(
            f"[{self.seen}/{self.total}] ({self.seen / self.total:.1%})",
            end="",
            flush=True,
        )


class StatsManager:
    def __init__(self):
        self.stats = {}

    def add(self, key, count=1):
        if key not in self.stats:
            self.stats[key] = 0
        self.stats[key] += count

    def print_stats(self):
        for key, value in self.stats.items():
            print(f"{key}: {value}")
