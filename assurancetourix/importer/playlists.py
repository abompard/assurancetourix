#!/usr/bin/env python3

"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class PlaylistImporter:
    def __init__(
        self,
        client,
        date=None,
        dbfile=None,
        verbose=False,
    ):
        self.client = client
        self.date = date
        self.dbfile = dbfile

    def run(self):
        playlist = self.client.get_or_create("parties", {"date": self.date})
        # current = self.client.call_api("songs/", data=params, method="POST")
        imported = self.client.call_api(
            "parties/{}/songs/".format(playlist["id"]),
            files={"performousdb": open(self.dbfile, "rb")},
            method="PUT",
        )
        print(f"Imported a playlist of {len(imported)} songs.")
