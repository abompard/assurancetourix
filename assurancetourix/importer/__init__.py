#!/usr/bin/env python3

"""
Assurancetourix - Karaoke party manager

Copyright (C) 2015  Aurelien Bompard <aurelien@bompard.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys

import click
from flask.cli import AppGroup

from .client import APIClient, APIError
from .playlists import PlaylistImporter
from .songs import SongsImporter


def _run_and_catch_errors(importer):
    try:
        importer.run()
    except (APIError, ConnectionError) as e:
        print(f"ERROR: {e}", file=sys.stderr)
        sys.exit(1)
    except KeyboardInterrupt:
        print("\rAborted on user request")
        sys.exit(1)


cli_group = AppGroup()


@cli_group.group("import")
@click.option("-u", "--url", help="URL of the Assurancetourix instance", required=True)
@click.option("-t", "--token", help="API token", required=True)
@click.option("--verbose", is_flag=True)
@click.option("--ignore-ssl-errors", is_flag=True, help="ignore SSL errors")
@click.pass_context
def cli(ctx, url, token, verbose, ignore_ssl_errors):
    """Import data into Assurancetourix"""
    ctx.ensure_object(dict)
    ctx.obj["client"] = APIClient(url, token, ignore_ssl_errors, verbose)
    ctx.obj["verbose"] = verbose


@cli.command("songs")
@click.option(
    "--add",
    is_flag=True,
    help="only add songs, don't delete songs not present locally",
)
@click.option(
    "--dry-run",
    is_flag=True,
    help="don't change the server database",
)
@click.argument("directory", nargs=-1, type=click.Path(exists=True))
@click.pass_context
def import_songs(ctx, add, dry_run, directory):
    """Import songs from directories on the disk."""
    _run_and_catch_errors(
        SongsImporter(
            client=ctx.obj["client"],
            directories=directory,
            add_only=add,
            dry_run=dry_run,
            verbose=ctx.obj["verbose"],
        )
    )


@cli.command("performousdb")
@click.option("-d", "--date", help="Date when this party happened", required=True)
@click.argument(
    "dbfile",
    type=click.Path(exists=True),
    default=os.path.expanduser("~/.config/performous/database.xml"),
)
@click.pass_context
def import_performousdb(ctx, date, dbfile):
    """Import Performous database.xml as a Karaoke Party"""
    _run_and_catch_errors(
        PlaylistImporter(
            client=ctx.obj["client"],
            date=date,
            dbfile=dbfile,
            verbose=ctx.obj["verbose"],
        )
    )
