from calendar import timegm
from email.utils import formatdate

import flask
from flask_restful import fields


class StaticUrl(fields.Url):
    def __init__(self, field_name, **kwargs):
        super().__init__(endpoint="static", **kwargs)
        self.field_name = field_name

    def output(self, key, obj):
        try:
            value = getattr(obj, self.field_name)
            return flask.url_for(self.endpoint, filename=value)
        except TypeError as te:
            raise fields.MarshallingException(te)


class Date(fields.DateTime):
    def format(self, value):
        try:
            if self.dt_format == "rfc822":
                return formatdate(timegm(value.timetuple()))
            elif self.dt_format == "iso8601":
                return value.isoformat()
            else:
                raise fields.MarshallingException(
                    "Unsupported date format %s" % self.dt_format
                )
        except AttributeError as ae:
            raise fields.MarshallingException(ae)


artist_fields = {
    "id": fields.Integer(attribute="artist_id"),
    "name": fields.String,
    "uri": fields.Url(".artistv1"),
    "songs": fields.Url(".artistsongslistv1"),
}

song_fields = {
    "uri": fields.Url(".songv1"),
    "id": fields.Integer(attribute="song_id"),
    "artist": fields.Nested(artist_fields),
    "title": fields.String,
    "genre": fields.String(attribute="genre.name"),
    "language": fields.String(attribute="language.name"),
    "cover": fields.String,
    "date_added": fields.DateTime("iso8601"),
    "date_updated": fields.DateTime("iso8601"),
    "duo": fields.Boolean,
    "has_karaoke": fields.Boolean,
}

genre_fields = {
    "id": fields.Integer(attribute="genre_id"),
    "name": fields.String,
    "uri": fields.Url(".genrev1"),
}

language_fields = {
    "id": fields.Integer,
    "name": fields.String,
    "uri": fields.Url(".languagev1"),
}

party_fields = {
    "uri": fields.Url(".partyv1"),
    "id": fields.Integer(),
    "date": Date("iso8601"),
    "songs": fields.Url(".partysongslistv1"),
}
