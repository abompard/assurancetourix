import datetime

import defusedxml.ElementTree as ET
from flask_restful import Resource, marshal_with, reqparse
from werkzeug.datastructures import FileStorage

from assurancetourix.database import db
from assurancetourix.models import Artist, Party, PlaylistEntry, Song
from assurancetourix.utils import SHORTTITLE_RE

from .serializers import party_fields, song_fields
from .utils import paginate_query, string_to_date, token_auth

parser = reqparse.RequestParser()
parser.add_argument("date", type=string_to_date, location="args")
parser.add_argument("performousdb", type=FileStorage, location="files")

PREFIXES = ("The", "Le", "La", "A")


def set_playlist(party, songs):
    party.playlist = [
        PlaylistEntry(party=party, song=song, order=index + 1)
        for index, song in enumerate(songs)
    ]
    db.session.flush()


def normalize_name(name, is_title=False):
    for prefix in PREFIXES:
        if name.endswith(f",{prefix}"):
            name = f"{name[-len(prefix):]} {name[:-len(prefix)-1]}"
    if is_title:
        mo = SHORTTITLE_RE.match(name)
        if mo:
            name = mo.group(1)
    return name


def get_songs_from_performous(performousdb):
    if not performousdb:
        return []
    all_songs = {}
    played = []
    try:
        tree = ET.parse(performousdb)
    except ET.ParseError as e:
        print("Can't parse performous db:")
        print(e)
        print(performousdb.read())
        return []
    for song in tree.getroot().find("songs").findall("song"):
        artist_name = normalize_name(song.attrib["artist"])
        song_title = normalize_name(song.attrib["title"], is_title=True)
        found = (
            db.session.query(Song)
            .join(Artist)
            .filter(Song.title == song_title, Artist.name == artist_name)
            .one_or_none()
        )
        if found is None:
            print(f"Can't find the song {artist_name} - {song_title}")
            continue
        all_songs[song.attrib["id"]] = found
    prev_play = None
    for play in sorted(
        tree.getroot().find("hiscores").findall("hiscore"),
        key=lambda p: int(p.attrib["unixtime"]),
    ):
        if (
            prev_play is not None
            and prev_play.attrib["songid"] == play.attrib["songid"]
            and int(play.attrib["unixtime"]) - int(prev_play.attrib["unixtime"]) < 30
        ):
            # it's the same song again because there were 2 players
            continue
        played.append(all_songs[play.attrib["songid"]])
        prev_play = play
    return played


class PartyV1(Resource):
    method_decorators = {"post": [token_auth], "delete": [token_auth]}

    @marshal_with(song_fields)
    def get(self, song_id):
        song = db.session.query(Song).get(song_id)
        return song

    def delete(self, party_id):
        party = db.session.query(Party).get(party_id)
        if party is None:
            return "No such party", 404
        db.session.delete(party)
        db.session.flush()
        db.session.commit()
        return "", 204

    @marshal_with(party_fields)
    def post(self, party_id):
        args = parser.parse_args()
        party = db.session.query(Party).get(party_id)
        set_playlist(party, get_songs_from_performous(args.performousdb))
        db.session.commit()
        db.session.refresh(party)
        return party, 201


class PartyListV1(Resource):
    method_decorators = {"post": [token_auth], "delete": [token_auth]}

    @paginate_query(party_fields)
    def get(self):
        return db.session.query(Party).order_by(Party.date)

    @marshal_with(party_fields)
    def post(self):
        args = parser.parse_args()
        party = Party(date=args["date"] or datetime.date.today())
        db.session.add(party)
        db.session.flush()
        set_playlist(party, get_songs_from_performous(args.performousdb))
        db.session.commit()
        db.session.refresh(party)
        return party, 201

    def delete(self):
        db.session.query(Party).delete()
        db.session.commit()
        return "", 204


class PartySongsListV1(Resource):
    def _get_songs_query(self, party_id):
        return (
            db.session.query(Song)
            .join(PlaylistEntry)
            .filter(PlaylistEntry.party_id == party_id)
            .order_by(PlaylistEntry.order)
        )

    @paginate_query(song_fields)
    def get(self, party_id):
        return self._get_songs_query(party_id)

    @paginate_query(song_fields)
    def put(self, party_id):
        party = db.session.query(Party).get(party_id)
        parser = reqparse.RequestParser()
        parser.add_argument(
            "performousdb", type=FileStorage, location="files", required=True
        )
        args = parser.parse_args()
        set_playlist(party, get_songs_from_performous(args.performousdb))
        db.session.commit()
        return self._get_songs_query(party_id)


search_parser = reqparse.RequestParser()
search_parser.add_argument("date", type=string_to_date, location="args")


class PartySearch(Resource):
    @paginate_query(party_fields)
    def get(self):
        args = search_parser.parse_args()
        parties = db.session.query(Party)
        if args.get("date"):
            parties = parties.filter(Party.date == args["date"])
        parties = parties.order_by(Party.date)
        return parties
