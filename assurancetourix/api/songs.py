from flask_restful import Resource, marshal_with, reqparse
from werkzeug.datastructures import FileStorage

from assurancetourix.database import db
from assurancetourix.models import Artist, Genre, Language, SearchTerm, Song
from assurancetourix.utils import SHORTTITLE_RE

from .serializers import song_fields
from .utils import clear_all, paginate_query, timestamp_to_date, token_auth


def convert_to_artist(name):
    return db.session.query(Artist).filter_by(name=name).one()


def convert_to_genre(name):
    if not name:
        return None
    return db.session.query(Genre).filter_by(name=name).one()


def convert_to_language(name):
    if not name:
        return None
    return db.session.query(Language).filter_by(name=name).one()


def convert_to_bool(value):
    if value.lower() in ("false", "f", "0"):
        return False
    return bool(value)


def shorten_title(value):
    if not value:
        return None
    mo = SHORTTITLE_RE.match(value)
    if mo:
        return mo.group(1)
    else:
        return value


parser = reqparse.RequestParser()
parser.add_argument("artist", type=convert_to_artist)
parser.add_argument("title")
parser.add_argument("genre", type=convert_to_genre)
parser.add_argument("language", type=convert_to_language)
parser.add_argument("date_added", type=timestamp_to_date)
parser.add_argument("date_updated", type=timestamp_to_date)
parser.add_argument("duo", type=convert_to_bool)
parser.add_argument("has_karaoke", type=convert_to_bool)
parser.add_argument("cover", type=FileStorage, location="files")


def update_song(song, args):
    for arg, value in args.items():
        if value is not None:
            setattr(song, arg, value)
        if arg == "genre" and value is None:
            old_genre = song.genre
            song.genre = None
            # Delete genres without songs.
            if old_genre is not None and len(old_genre.songs) == 0:
                db.session.delete(old_genre)
        if arg == "language" and value is None:
            old_value = song.language
            song.language = None
            # Delete languages without songs.
            if old_value is not None and len(old_value.songs) == 0:
                db.session.delete(old_value)
    song.update_search_terms()


class SongV1(Resource):
    method_decorators = {"patch": [token_auth], "delete": [token_auth]}

    @marshal_with(song_fields)
    def get(self, song_id):
        song = db.session.query(Song).get(song_id)
        return song

    def delete(self, song_id):
        song = db.session.query(Song).get(song_id)
        if song is None:
            return "No such song", 404
        artist = song.artist
        genre = song.genre
        language = song.language
        db.session.delete(song)
        db.session.flush()
        # Delete artists without songs.
        if len(artist.songs) == 0:
            db.session.delete(artist)
        # Delete genres and languages without songs.
        if genre is not None and len(genre.songs) == 0:
            db.session.delete(genre)
        if language is not None and len(language.songs) == 0:
            db.session.delete(language)
        db.session.commit()
        return "", 204

    @marshal_with(song_fields)
    def patch(self, song_id):
        args = parser.parse_args()
        song = db.session.query(Song).get(song_id)
        update_song(song, args)
        db.session.commit()
        db.session.refresh(song)
        return song, 201


class SongListV1(Resource):
    method_decorators = {"post": [token_auth], "delete": [token_auth]}

    @paginate_query(song_fields)
    def get(self):
        return db.session.query(Song).order_by(Song.title)

    @marshal_with(song_fields)
    def post(self):
        parser_post = parser.copy()
        for arg in parser_post.args:
            if arg.name in ("artist", "title"):
                arg.required = True
        args = parser_post.parse_args()
        song = Song(artist=args["artist"], title=args["title"])
        db.session.add(song)
        update_song(song, args)
        db.session.commit()
        db.session.refresh(song)
        return song, 201

    def delete(self):
        clear_all()
        db.session.commit()
        return "", 204


search_parser = reqparse.RequestParser()
search_parser.add_argument("artist")
search_parser.add_argument("title")
search_parser.add_argument("contains")
search_parser.add_argument("genre")
search_parser.add_argument("language")
search_parser.add_argument("duo", type=bool)
search_parser.add_argument("karaoke", type=bool)


class SongSearch(Resource):
    @paginate_query(song_fields)
    def get(self):
        args = search_parser.parse_args()
        songs = db.session.query(Song)
        if args.get("artist"):
            songs = songs.filter(Artist.name == args["artist"])
        if args.get("title"):
            songs = songs.filter_by(title=args["title"])
        if args.get("contains"):
            songs = (
                songs.join(SearchTerm)
                .filter(SearchTerm.terms.like("%{}%".format(args["contains"].lower())))
                .distinct()
            )
        if args.get("duo", False):
            songs = songs.filter(Song.duo.is_(True))
        if args.get("karaoke", False):
            songs = songs.filter(Song.has_karaoke.is_(True))
        songs = songs.join(Artist).order_by(Artist.sort_as, Song.title)
        return songs
