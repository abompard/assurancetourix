from flask_restful import Resource, marshal_with, reqparse

from assurancetourix.database import db
from assurancetourix.models import Language

from .serializers import language_fields
from .utils import paginate_query, token_auth

parser = reqparse.RequestParser()
parser.add_argument("name")


class LanguageV1(Resource):

    method_decorators = {"patch": [token_auth], "delete": [token_auth]}

    @marshal_with(language_fields)
    def get(self, id):
        language = db.session.query(Language).get(id)
        return language

    def delete(self, language_id):
        language = db.session.query(Language).get(language_id)
        language.delete()
        return "", 204

    @marshal_with(language_fields)
    def patch(self, language_id):
        args = parser.parse_args()
        language = db.session.query(Language).get(language_id)
        language.name = args["name"]
        db.session.commit()
        db.session.refresh(language)
        return language, 201


class LanguageListV1(Resource):

    method_decorators = {"post": [token_auth]}

    @paginate_query(language_fields)
    def get(self):
        return db.session.query(Language).order_by(Language.name)

    @marshal_with(language_fields)
    def post(self):
        args = parser.parse_args()
        language = Language(**args)
        db.session.add(language)
        db.session.commit()
        db.session.refresh(language)
        return language, 201


class LanguageSearch(Resource):
    @paginate_query(language_fields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        args = parser.parse_args()
        languages = db.session.query(Language)
        if args.get("name"):
            languages = languages.filter_by(name=args["name"])
        return languages
