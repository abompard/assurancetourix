import datetime
import os
import shutil
from functools import wraps
from urllib.parse import parse_qs, urlencode, urlparse, urlunparse

from flask import current_app, request
from flask_restful import abort, marshal

from assurancetourix.database import db
from assurancetourix.models import Artist, Genre, Language, Song
from assurancetourix.utils import get_cover_folder


class paginate_query:
    def __init__(self, fields=None):
        self.fields = fields

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            query = f(*args, **kwargs)
            return pagination(query, self.fields)

        return wrapper


def pagination(query, fields=None):
    total = query.count()
    try:
        start = int(request.args.get("start", "0"))
    except (TypeError, ValueError):
        start = 0
    try:
        batch_size = int(request.args.get("max"))
    except (TypeError, ValueError):
        batch_size = current_app.config["DISPLAY_BATCH_SIZE"]
    end = start + batch_size
    if end >= total:
        next_start = None
    else:
        next_start = end
    if next_start:
        url = urlparse(request.url)
        next_qs = parse_qs(url.query)
        next_qs["start"] = next_start
        next_url = urlunparse(url[:4] + (urlencode(next_qs, doseq=True), ""))
    else:
        next_url = None
    contents = query[start:end]
    if fields is not None:
        contents = marshal(contents, fields)
    return {
        "total": total,
        "start": start,
        "results": contents,
        "next": next_start,
        "next_url": next_url,
    }


def clear_all():
    db.session.query(Song).delete()
    db.session.query(Artist).delete()
    db.session.query(Genre).delete()
    db.session.query(Language).delete()
    # Clean all covers
    cover_folder = get_cover_folder()
    for subdir in os.listdir(cover_folder):
        subdir_path = os.path.join(cover_folder, subdir)
        if os.path.isdir(subdir_path):
            shutil.rmtree(subdir_path)


def token_auth(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        token = request.values.get("api_token")
        if not token and "Authorization" in request.headers:
            token = request.headers["Authorization"].split(" ")[1]
        if token != current_app.config.get("API_TOKEN"):
            abort(401)
        return func(*args, **kwargs)

    return wrapper


def timestamp_to_date(value):
    return datetime.datetime.utcfromtimestamp(int(value))


def string_to_date(value):
    return datetime.date.fromisoformat(value)
