from flask_restful import Resource, marshal_with, reqparse

from assurancetourix.database import db
from assurancetourix.models import Genre

from .serializers import genre_fields
from .utils import paginate_query, token_auth

parser = reqparse.RequestParser()
parser.add_argument("name")


class GenreV1(Resource):

    method_decorators = {"patch": [token_auth], "delete": [token_auth]}

    @marshal_with(genre_fields)
    def get(self, genre_id):
        genre = db.session.query(Genre).get(genre_id)
        return genre

    def delete(self, genre_id):
        genre = db.session.query(Genre).get(genre_id)
        genre.delete()
        return "", 204

    @marshal_with(genre_fields)
    def patch(self, genre_id):
        args = parser.parse_args()
        genre = db.session.query(Genre).get(genre_id)
        genre.name = args["name"]
        db.session.commit()
        db.session.refresh(genre)
        return genre, 201


class GenreListV1(Resource):

    method_decorators = {"post": [token_auth]}

    @paginate_query(genre_fields)
    def get(self):
        return db.session.query(Genre).order_by(Genre.name)

    @marshal_with(genre_fields)
    def post(self):
        args = parser.parse_args()
        genre = Genre(**args)
        db.session.add(genre)
        db.session.commit()
        db.session.refresh(genre)
        return genre, 201


class GenreSearch(Resource):
    @paginate_query(genre_fields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        args = parser.parse_args()
        genres = db.session.query(Genre)
        if args.get("name"):
            genres = genres.filter_by(name=args["name"])
        return genres
