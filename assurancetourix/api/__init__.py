from flask import Blueprint
from flask_restful import Api

from .artists import ArtistListV1, ArtistSearch, ArtistSongsListV1, ArtistV1
from .genres import GenreListV1, GenreSearch, GenreV1
from .languages import LanguageListV1, LanguageSearch, LanguageV1
from .parties import PartyListV1, PartySearch, PartySongsListV1, PartyV1
from .songs import SongListV1, SongSearch, SongV1

api_bp = Blueprint("api", __name__)
api = Api(api_bp)

api.add_resource(ArtistListV1, "/v1/artists/")
api.add_resource(ArtistV1, "/v1/artists/<int:artist_id>")
api.add_resource(ArtistSongsListV1, "/v1/artists/<int:artist_id>/songs/")
api.add_resource(ArtistSearch, "/v1/artists/search")

api.add_resource(SongListV1, "/v1/songs/")
api.add_resource(SongV1, "/v1/songs/<int:song_id>")
api.add_resource(SongSearch, "/v1/songs/search")

api.add_resource(GenreListV1, "/v1/genres/")
api.add_resource(GenreV1, "/v1/genres/<int:genre_id>")
api.add_resource(GenreSearch, "/v1/genres/search")

api.add_resource(LanguageListV1, "/v1/languages/")
api.add_resource(LanguageV1, "/v1/languages/<int:id>")
api.add_resource(LanguageSearch, "/v1/languages/search")

api.add_resource(PartyListV1, "/v1/parties/")
api.add_resource(PartyV1, "/v1/parties/<int:party_id>")
api.add_resource(PartySongsListV1, "/v1/parties/<int:party_id>/songs/")
api.add_resource(PartySearch, "/v1/parties/search")
