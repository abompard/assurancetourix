from flask_restful import Resource, marshal_with, reqparse

from assurancetourix.database import db
from assurancetourix.models import Artist, Song

from .serializers import artist_fields, song_fields
from .utils import paginate_query, token_auth

parser = reqparse.RequestParser()
parser.add_argument("name", required=True)


class ArtistV1(Resource):

    method_decorators = {
        "put": [token_auth],
        "patch": [token_auth],
        "delete": [token_auth],
    }

    @marshal_with(artist_fields)
    def get(self, artist_id):
        artist = db.session.query(Artist).get(artist_id)
        return artist

    def delete(self, artist_id):
        artist = db.session.query(Artist).get(artist_id)
        artist.delete()
        return "", 204

    @marshal_with(artist_fields)
    def put(self, artist_id):
        args = parser.parse_args()
        artist = db.session.query(Artist).get(artist_id)
        artist.name = args["name"]
        db.session.commit()
        return artist, 201

    @marshal_with(artist_fields)
    def patch(self, artist_id):
        parser_patch = parser.copy()
        parser_patch.replace_argument("name")
        args = parser_patch.parse_args()
        artist = db.session.query(Artist).get(artist_id)
        if args["name"]:
            artist.name = args["name"]
        db.session.commit()
        db.session.refresh(artist)
        return artist, 201


class ArtistListV1(Resource):

    method_decorators = {"post": [token_auth]}

    @paginate_query(artist_fields)
    def get(self):
        return db.session.query(Artist).order_by(Artist.sort_as)

    @marshal_with(artist_fields)
    def post(self):
        args = parser.parse_args()
        artist = Artist(**args)
        db.session.add(artist)
        db.session.commit()
        db.session.refresh(artist)
        return artist, 201


class ArtistSongsListV1(Resource):
    @paginate_query(song_fields)
    def get(self, artist_id):
        songs = db.session.query(Song).filter_by(artist_id=artist_id)
        return songs


class ArtistSearch(Resource):
    @paginate_query(artist_fields)
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("contains")
        args = parser.parse_args()
        artists = db.session.query(Artist)
        if args.get("name"):
            artists = artists.filter_by(name=args["name"])
        if args.get("contains"):
            artists = artists.filter(
                Artist.name.like("%{}%".format(args["contains"].lower()))
            ).distinct()
        return artists
