"""Split off the Genre

Revision ID: 3b77e67ebe60
Revises: 4459c8f139bb
Create Date: 2015-10-03 16:27:08.778320

"""

import sqlalchemy as sa
from alembic import op

from assurancetourix.database import exists_in_db, is_sqlite

# revision identifiers, used by Alembic.
revision = "3b77e67ebe60"
down_revision = "4459c8f139bb"
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    genres_table = op.create_table(
        "genres",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Unicode(length=254), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_genres")),
    )
    op.create_index(op.f("ix_genres_name"), "genres", ["name"], unique=True)
    op.add_column("songs", sa.Column("genre_id", sa.Integer(), nullable=True))
    op.drop_index("ix_songs_genre", table_name="songs")
    if not is_sqlite(conn):
        op.create_foreign_key(
            op.f("fk_songs_genre_id_genres"),
            "songs",
            "genres",
            ["genre_id"],
            ["id"],
            onupdate="cascade",
            ondelete="set null",
        )
    # Data migration
    songs_table = sa.sql.table(
        "songs",
        sa.sql.column("id", sa.Integer),
        sa.sql.column("genre", sa.Unicode),
        sa.sql.column("genre_id", sa.Integer),
    )
    for song in conn.execute(songs_table.select()).fetchall():
        if not song["genre"]:
            continue
        result = conn.execute(
            genres_table.select().where(genres_table.c.name == song["genre"])
        ).fetchone()
        if result is None:
            result = conn.execute(genres_table.insert().values(name=song["genre"]))
            genre_id = result.inserted_primary_key[0]
        else:
            genre_id = result["id"]
        conn.execute(songs_table.update().values(genre_id=genre_id))
    if not is_sqlite(conn):
        op.drop_column("songs", "genre")


def downgrade():
    conn = op.get_bind()
    if not exists_in_db(conn, "songs", "genre"):
        op.add_column(
            "songs", sa.Column("genre", sa.VARCHAR(length=254), nullable=True)
        )
        op.create_index("ix_songs_genre", "songs", ["genre"], unique=False)
    songs_table = sa.sql.table(
        "songs",
        sa.sql.column("id", sa.Integer),
        sa.sql.column("genre", sa.Unicode),
        sa.sql.column("genre_id", sa.Integer),
    )
    genres_table = sa.sql.table(
        "genres", sa.sql.column("id", sa.Integer), sa.sql.column("name", sa.Unicode)
    )
    for song in conn.execute(
        sa.select([songs_table.c.id, genres_table.c.name]).select_from(
            songs_table.join(genres_table, songs_table.c.genre_id == genres_table.c.id)
        )
    ).fetchall():
        conn.execute(
            songs_table.update()
            .values(genre=song["name"])
            .where(songs_table.c.id == song["id"])
        )
    if not is_sqlite(conn):
        op.drop_constraint(
            op.f("fk_songs_genre_id_genres"), "songs", type_="foreignkey"
        )
        op.drop_column("songs", "genre_id")
    op.drop_index(op.f("ix_genres_name"), table_name="genres")
    op.drop_table("genres")
