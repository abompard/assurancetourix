"""Search terms table

Revision ID: 6c8fa4d92c86
Revises: 3b77e67ebe60
Create Date: 2017-01-12 23:16:52.964405

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6c8fa4d92c86"
down_revision = "3b77e67ebe60"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "search_terms",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("song_id", sa.Integer(), nullable=False),
        sa.Column("terms", sa.Unicode(length=254), nullable=False),
        sa.ForeignKeyConstraint(
            ["song_id"],
            ["songs.id"],
            name=op.f("fk_search_terms_song_id_songs"),
            onupdate="cascade",
            ondelete="cascade",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_search_terms")),
    )
    op.create_index(
        op.f("ix_search_terms_terms"), "search_terms", ["terms"], unique=False
    )


def downgrade():
    op.drop_index(op.f("ix_search_terms_terms"), table_name="search_terms")
    op.drop_table("search_terms")
