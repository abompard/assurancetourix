"""Add parties

Revision ID: 5a417c6a9e66
Revises: c592c44dd147
Create Date: 2021-12-23 12:24:44.471506

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5a417c6a9e66"
down_revision = "c592c44dd147"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "parties",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("date", sa.Date(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_parties")),
    )
    op.create_index(op.f("ix_parties_date"), "parties", ["date"], unique=False)
    op.create_table(
        "playlist_entries",
        sa.Column("song_id", sa.Integer(), nullable=False),
        sa.Column("party_id", sa.Integer(), nullable=False),
        sa.Column("order", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["party_id"],
            ["parties.id"],
            name=op.f("fk_playlist_entries_party_id_parties"),
            onupdate="cascade",
            ondelete="cascade",
        ),
        sa.ForeignKeyConstraint(
            ["song_id"],
            ["songs.id"],
            name=op.f("fk_playlist_entries_song_id_songs"),
            onupdate="cascade",
            ondelete="cascade",
        ),
        sa.PrimaryKeyConstraint(
            "song_id", "party_id", "order", name=op.f("pk_playlist_entries")
        ),
    )
    op.create_index(
        op.f("ix_playlist_entries_order"), "playlist_entries", ["order"], unique=False
    )
    op.create_index(
        op.f("ix_playlist_entries_party_id"),
        "playlist_entries",
        ["party_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_playlist_entries_song_id"),
        "playlist_entries",
        ["song_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_playlist_entries_song_id"), table_name="playlist_entries")
    op.drop_index(op.f("ix_playlist_entries_party_id"), table_name="playlist_entries")
    op.drop_index(op.f("ix_playlist_entries_order"), table_name="playlist_entries")
    op.drop_table("playlist_entries")
    op.drop_index(op.f("ix_parties_date"), table_name="parties")
    op.drop_table("parties")
