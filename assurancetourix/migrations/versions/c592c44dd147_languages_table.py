"""Languages table

Revision ID: c592c44dd147
Revises: dbf847d5ee78
Create Date: 2021-11-28 11:47:50.535136

"""

import sqlalchemy as sa
from alembic import op

from assurancetourix.database import exists_in_db, is_sqlite

# revision identifiers, used by Alembic.
revision = "c592c44dd147"
down_revision = "dbf847d5ee78"
branch_labels = None
depends_on = None

songs_table = sa.sql.table(
    "songs",
    sa.sql.column("id", sa.Integer),
    sa.sql.column("language", sa.Unicode),
    sa.sql.column("language_id", sa.Integer),
)


def upgrade():
    conn = op.get_bind()
    languages_table = op.create_table(
        "languages",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Unicode(length=254), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_languages")),
    )
    op.create_index(op.f("ix_languages_name"), "languages", ["name"], unique=True)
    op.add_column("songs", sa.Column("language_id", sa.Integer(), nullable=True))
    op.drop_index("ix_songs_language", table_name="songs")
    with op.batch_alter_table("songs") as batch_op:
        batch_op.create_foreign_key(
            op.f("fk_songs_language_id_languages"),
            "languages",
            ["language_id"],
            ["id"],
            onupdate="cascade",
            ondelete="set null",
        )
    # Data migration
    for song in conn.execute(songs_table.select()).fetchall():
        if not song["language"]:
            continue
        result = conn.execute(
            languages_table.select().where(languages_table.c.name == song["language"])
        ).fetchone()
        if result is None:
            result = conn.execute(
                languages_table.insert().values(name=song["language"])
            )
            language_id = result.inserted_primary_key[0]
        else:
            language_id = result["id"]
        conn.execute(
            songs_table.update()
            .values(language_id=language_id)
            .filter(songs_table.c.id == song["id"])
        )
    if not is_sqlite(conn):
        op.drop_column("songs", "language")


def downgrade():
    conn = op.get_bind()
    if not exists_in_db(conn, "songs", "language"):
        op.add_column(
            "songs", sa.Column("language", sa.VARCHAR(length=254), nullable=True)
        )
        op.create_index("ix_songs_language", "songs", ["language"], unique=False)
    # Data migration
    languages_table = sa.sql.table(
        "languages", sa.sql.column("id", sa.Integer), sa.sql.column("name", sa.Unicode)
    )
    for song in conn.execute(
        sa.select([songs_table.c.id, languages_table.c.name]).select_from(
            songs_table.join(
                languages_table, songs_table.c.language_id == languages_table.c.id
            )
        )
    ).fetchall():
        conn.execute(
            songs_table.update()
            .values(language=song["name"])
            .where(songs_table.c.id == song["id"])
        )
    # ---
    with op.batch_alter_table("songs") as batch_op:
        batch_op.drop_constraint(
            op.f("fk_songs_language_id_languages"), type_="foreignkey"
        )
        batch_op.drop_column("language_id")
    op.drop_index(op.f("ix_languages_name"), table_name="languages")
    op.drop_table("languages")
