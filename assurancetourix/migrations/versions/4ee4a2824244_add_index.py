"""Add index

Revision ID: 4ee4a2824244
Revises: 491bc3d020f3
Create Date: 2015-10-02 15:42:38.547326

"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "4ee4a2824244"
down_revision = "491bc3d020f3"
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(op.f("ix_songs_date_added"), "songs", ["date_added"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_songs_date_added"), table_name="songs")
