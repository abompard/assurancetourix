"""Add indexes and sort_as

Revision ID: 438fcca3ba4a
Create Date: 2015-09-30 09:26:51.856727

"""

import sqlalchemy as sa
from alembic import op

from assurancetourix.database import is_sqlite
from assurancetourix.models import get_sort_string

# revision identifiers, used by Alembic.
revision = "438fcca3ba4a"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    if not is_sqlite(conn):
        op.alter_column(
            "songs", "artist", existing_type=sa.VARCHAR(length=254), nullable=False
        )
        op.alter_column(
            "songs", "title", existing_type=sa.VARCHAR(length=254), nullable=False
        )
    # don't import the table definition from the models, it may break this
    # migration when the model is updated in the future (see the Alembic doc)
    op.add_column("songs", sa.Column("sort_as", sa.Unicode(length=254), nullable=True))
    if not is_sqlite(conn):
        songs_table = sa.sql.table(
            "songs",
            sa.sql.column("id", sa.Integer),
            sa.sql.column("artist", sa.Unicode),
            sa.sql.column("title", sa.Unicode),
            sa.sql.column("sort_as", sa.Unicode),
        )
        for song in conn.execute(songs_table.select()).fetchall():
            sort_as = get_sort_string(song["artist"], song["title"])
            conn.execute(
                songs_table.update()
                .where(songs_table.c.id == song["id"])
                .values(sort_as=sort_as)
            )
        op.alter_column(
            "songs", "sort_as", existing_type=sa.VARCHAR(length=254), nullable=False
        )
    op.create_index(op.f("ix_songs_sort_as"), "songs", ["sort_as"], unique=False)
    if not is_sqlite(conn):
        op.create_unique_constraint(
            op.f("uq_songs_artist"), "songs", ["artist", "title"]
        )


def downgrade():
    conn = op.get_bind()
    if not is_sqlite(conn):
        op.drop_constraint(op.f("uq_songs_artist"), "songs", type_="unique")
        op.alter_column(
            "songs", "title", existing_type=sa.VARCHAR(length=254), nullable=True
        )
        op.alter_column(
            "songs", "artist", existing_type=sa.VARCHAR(length=254), nullable=True
        )
    op.drop_index(op.f("ix_songs_sort_as"), table_name="songs")
    if not is_sqlite(conn):
        op.drop_column("songs", "sort_as")
