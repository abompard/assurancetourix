"""Add Song.date_updated

Revision ID: dbf847d5ee78
Revises: 6c8fa4d92c86
Create Date: 2017-10-30 12:34:53.824746

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dbf847d5ee78"
down_revision = "6c8fa4d92c86"
branch_labels = None
depends_on = None


def upgrade():
    songs_table = sa.sql.table(
        "songs",
        sa.sql.column("date_added", sa.DateTime),
        sa.sql.column("date_updated", sa.DateTime),
    )
    op.add_column("songs", sa.Column("date_updated", sa.DateTime(), nullable=True))
    # op.add_column('songs', sa.Column(
    #    'date_updated', sa.DateTime(), nullable=False,
    #    default=songs_table.c.date_added))
    # conn = op.get_bind()
    # conn.execute(songs_table.update().values(date_updated=songs_table.c.date_added))
    op.execute(songs_table.update().values(date_updated=songs_table.c.date_added))
    with op.batch_alter_table("songs") as batch_op:
        batch_op.alter_column("date_updated", existing_type=sa.DateTime, nullable=False)
    op.create_index(
        op.f("ix_songs_date_updated"), "songs", ["date_updated"], unique=False
    )


def downgrade():
    op.drop_index(op.f("ix_songs_date_updated"), table_name="songs")
    with op.batch_alter_table("songs") as batch_op:
        batch_op.drop_column("date_updated")
