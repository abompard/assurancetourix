"""Add Song.duo

Revision ID: 4459c8f139bb
Revises: 4ee4a2824244
Create Date: 2015-10-03 10:38:38.640553

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4459c8f139bb"
down_revision = "4ee4a2824244"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "songs",
        sa.Column("duo", sa.Boolean(), server_default=sa.text("0"), nullable=False),
    )
    op.add_column(
        "songs",
        sa.Column(
            "has_karaoke", sa.Boolean(), server_default=sa.text("0"), nullable=False
        ),
    )


def downgrade():
    op.drop_column("songs", "has_karaoke")
    op.drop_column("songs", "duo")
