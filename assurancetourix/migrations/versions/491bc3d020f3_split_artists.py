"""Split artists

Revision ID: 491bc3d020f3
Revises: 438fcca3ba4a
Create Date: 2015-10-01 16:04:05.406991

"""

import sqlalchemy as sa
from alembic import op

from assurancetourix.database import is_sqlite

# revision identifiers, used by Alembic.
revision = "491bc3d020f3"
down_revision = "438fcca3ba4a"
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    op.create_table(
        "artists",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Unicode(length=254), nullable=False),
        sa.Column("sort_as", sa.Unicode(length=254), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_artists")),
    )
    op.create_index(op.f("ix_artists_name"), "artists", ["name"], unique=True)
    op.create_index(op.f("ix_artists_sort_as"), "artists", ["sort_as"], unique=False)
    op.add_column("songs", sa.Column("artist_id", sa.Integer(), nullable=False))
    op.create_foreign_key(
        op.f("fk_songs_artist_id_artists"),
        "songs",
        "artists",
        ["artist_id"],
        ["id"],
        onupdate="cascade",
        ondelete="cascade",
    )
    op.create_unique_constraint(
        op.f("uq_songs_artist_id"), "songs", ["artist_id", "title"]
    )
    # Data migration not supported, just recreate the library
    if not is_sqlite(conn):
        op.alter_column(
            "songs", "title", existing_type=sa.VARCHAR(length=254), nullable=False
        )
        op.drop_index("ix_songs_artist", table_name="songs")
        op.drop_index("ix_songs_sort_as", table_name="songs")
        op.drop_column("songs", "sort_as")
        op.drop_column("songs", "artist")


def downgrade():
    raise RuntimeError("Downgrade not supported")
