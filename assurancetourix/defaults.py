import os

basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "..", "asstx.db")
BROWSERID_AUDIENCE = ["http://127.0.0.1:5000", "http://127.0.0.1:5001"]
SECRET_KEY = "SomethingVerySecretThatYouMustChange"  # noqa: S105
ADMINS = ("you@example.com",)
TEMPLATE_DIRS = []
MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # Don't upload more than 16MB
COVERS_DIR = "covers"
COVERS_SIZE = 70
DISPLAY_BATCH_SIZE = 20
API_TOKEN = "change-me"  # noqa: S105
